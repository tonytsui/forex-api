document.getElementById("getRates").addEventListener("click", getRates);

let combined = [];

// Fetch API data
function getRates() {
  let fetchData = fetch(
    "http://data.fixer.io/api/latest?access_key=fe3a9b3f9ac173a3e51d8ea22951cf95"
  );
  fetchData
    .then((res) => res.json())
    .then((data) => {
      let entries = Object.entries(data.rates);
      entries.map((entry) => {
        let adj = entry[1] + 10.0002;
        combined.push({
          cur: entry[0],
          raw: entry[1],
          adj,
          check: Math.floor(adj) % 2 == 0 ? "Yes" : "No",
        });
      });

// Display variables in a table
      const currencyList = document.querySelector("#table");
      let s = `<tr>
    <th>Currency Name</th>
    <th>Raw Exchange Rates</th>
    <th>Adjusted Exchange Rates</th>
    <th>Even Number Check</th>
    </tr>`;
      for (let currency of combined) {
        s += `
            <tr>
                <td ${
                  currency.cur == "HKD" ? 'style="outline:1px red solid;"' : ""
                }> ${currency.cur} </td>
                <td> ${currency.raw} </td>
                <td ${
                  currency.check == "Yes"
                    ? 'style="outline:1px red solid;"'
                    : ""
                }> ${currency.adj} </td>
                <td> ${currency.check} </td>
            </tr>
        `;
      }
      currencyList.innerHTML = s;
    });
}
