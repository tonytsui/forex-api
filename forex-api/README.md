To-do list:
[X] - Make a GET REST to http://data.fixer.io/api/latest?access_key=fe3a9b3f9ac173a3e51d8ea22951cf95 and then retrieve the foreign currency data and save it to variable. ( if the access_key isn’t usable, please create a new access_key by registration a free account on their website ) 
[X] - Create a new variable with the previous obtained data from the REST API call, but in this new variable, add 10.0002 to each values of those currencies.
[X] - Display both variables in a table
[X] - For those values where its value is of even number, add a border colour of red to the table data. For those value where the currency is HKD, add a border colour of red to the table data as well.
[X] - Create a function to check if the value is an even number.
